## git-tutorial

Collaborative for git tutorial Ibra

Toy shell for git tutorial purposes

Fork this repository, and then clone it by running the following command:

    git clone git@bitbucket.org:<username>/git-tutorial-code

### C++
* Building:

      cd cpp
      mkdir build
      cd build
      cmake ../
      make

* Running:

      ./lust

### Python

* Running:

      cd python
      ./lust.py

### Maintainers

Gianluca Sampietro
